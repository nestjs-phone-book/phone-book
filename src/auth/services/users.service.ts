import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { UsersRepository } from '../repositories/users.repository';
import { Profile } from './oauth/profile.class';
import { Provider } from './oauth/provider.enum';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(UsersRepository) private usersRepository: UsersRepository,
  ) { }

  public async findById(id: number): Promise<User> {
    return await this.usersRepository.findOneOrFail(id);
  }

  public async findByEmail(email: string): Promise<User> {
    return await this.usersRepository.getByEmail(email);
  }

  public async createUser(profile: Profile, provider: Provider, password: string = null): Promise<User> {
    const user = new User();
    user.email = profile.email;
    user.name = profile.name;
    user.profilePicture = profile.profilePicture;
    user.providerId = profile.id;
    user.provider = provider;
    user.password = password;
    return await this.usersRepository.save(user);
  }
}
