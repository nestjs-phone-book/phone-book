import { ProfileParser } from './profile-parser.class';

export class GoogleProfileParser extends ProfileParser {

  constructor(data: any) {
    super(data);
    const pdata = data._json;
    this.email = pdata.email;
    this.name = pdata.name;
    this.profilePicture = pdata.picture;
    this.id = pdata.sub;
  }
}
