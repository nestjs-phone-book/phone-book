import { ProfileParser } from './profile-parser.class';
import { SignupRequestDto } from '../../dto/signup-request.dto';

export class SignUpRequestProfileParser extends ProfileParser {

  constructor(data: SignupRequestDto) {
    super(data);
    this.email = data.email;
    this.name = data.name;
    this.profilePicture = null;
    this.id = null;
  }
}
