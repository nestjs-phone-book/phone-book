export abstract class ProfileParser {
  email: string;
  name: string;
  profilePicture: string;
  id: string;

  // tslint:disable-next-line:no-empty
  protected constructor(data: any) {}

}
