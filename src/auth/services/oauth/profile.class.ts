import { Provider } from './provider.enum';
import { ProfileParser } from './profile-parser.class';
import { ProfileParserFactory } from './profile-parser-factory.class';

export class Profile {
  email: string;
  name: string;
  profilePicture: string;
  id: string;

  private parser: ProfileParser;

  constructor(data: any, provider: Provider) {
    this.parseProfile(data, provider);
    this.email = this.parser.email;
    this.name = this.parser.name;
    this.profilePicture = this.parser.profilePicture;
    this.id = this.parser.id;
  }

  private parseProfile(data: any, provider: Provider) {
    this.parser = ProfileParserFactory.makeInstance(data, provider);
  }
}
