import { Provider } from './provider.enum';
import { GoogleProfileParser } from './google-profile-parser.class';
import { ProfileParser } from './profile-parser.class';
import { SignUpRequestProfileParser } from './signup-request-profile-parser.class';

export class ProfileParserFactory {
  private constructor() {}

  public static makeInstance(data: any, provider: Provider): ProfileParser {
    switch (provider) {
      case Provider.GOOGLE:
        return new GoogleProfileParser(data);
      case Provider.LOCAL:
        return new SignUpRequestProfileParser(data);
      default:
        return new GoogleProfileParser(data);
    }
  }
}
