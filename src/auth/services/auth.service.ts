import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { sign } from 'jsonwebtoken';
import { Provider } from './oauth/provider.enum';
import { Profile } from './oauth/profile.class';
import { User } from '../entities/user.entity';
import { UsersService } from './users.service';
import { SignupRequestDto } from '../dto/signup-request.dto';

import * as bcrypt from 'bcrypt';
import { LoginRequestDto } from '../dto/login-request.dto';

@Injectable()
export class AuthService {

  private readonly JWT_SECRET_KEY = 'VERY_SECRET_KEY'; // <- replace this with your secret key

  // tslint:disable-next-line:no-empty
  constructor(private readonly usersService: UsersService) {}

  async performOAuthLogin(profileObject: any, provider: Provider): Promise<string> {
    const profile: Profile = new Profile(profileObject, provider);

    // Check for user;
    let user: User = await this.usersService.findByEmail(profile.email);
    if (typeof user === 'undefined') {
      user = await this.usersService.createUser(profile, provider);
    } else {
      this.validateProvider(provider, user.provider);
    }

    return await this.createToken(user);
  }

  async performLocalSignup(signupRequestDto: SignupRequestDto): Promise<{token: string}> {
    const profile: Profile = new Profile(signupRequestDto, Provider.LOCAL);

    // Check for user;
    const user: User = await this.usersService.createUser(
      profile,
      Provider.LOCAL,
      this.hashPassword(signupRequestDto.password));

    return { token: await this.createToken(user) };
  }

  async performLocalLogin(loginRequestDto: LoginRequestDto): Promise<{token: string}> {

    // Check for user;
    const user: User = await this.usersService.findByEmail(loginRequestDto.email);
    if (typeof user === 'undefined') {
      throw new UnauthorizedException('unknown user');
    }
    this.comparePassword(loginRequestDto.password, user.password);
    return { token: await this.createToken(user) };
  }

  private hashPassword(password: string) {
    return bcrypt.hashSync(password, 10);
  }

  private comparePassword(plain: string, hash: string) {
    try {
      if (!bcrypt.compareSync(plain, hash)) {
        throw new UnauthorizedException('invalid credentials');
      }
    } catch (e) {
      throw new UnauthorizedException('invalid credentials');
    }
  }

  private validateProvider(requestProvider: Provider , userProvider: Provider) {
    if (requestProvider !== userProvider) {
      throw new BadRequestException('email already registered with other provider');
    }
  }
  private async createToken(user: User): Promise<string> {

    delete user.password;
    const payload = {
      user,
    };

    return sign(payload, this.JWT_SECRET_KEY, { expiresIn: 3600 });
  }

}
