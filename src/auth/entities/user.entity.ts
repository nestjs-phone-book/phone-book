import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Provider } from '../services/oauth/provider.enum';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: true})
  email: string;

  @Column({default: null, nullable: true})
  password: string;

  @Column()
  name: string;

  @Column({default: null, nullable: true})
  profilePicture: string;

  @Column()
  provider: Provider;

  @Column({name : 'provider_id', default: null, nullable: true})
  providerId: string;

}
