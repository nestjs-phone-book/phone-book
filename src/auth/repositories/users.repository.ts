import {EntityRepository, Repository} from 'typeorm';
import { User } from '../entities/user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {

  getByEmail(email: string): Promise<User> {
    return this.createQueryBuilder()
      .where('email = :email', {email}).getOne();
  }
}
