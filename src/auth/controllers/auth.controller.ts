import { Controller, Get, UseGuards, Res, Req, Post, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from '../utils/decorators/current-user';
import { SignupRequestDto } from '../dto/signup-request.dto';
import { AuthService } from '../services/auth.service';
import { LoginRequestDto } from '../dto/login-request.dto';

@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService) {}

  @Get('google')
  @UseGuards(AuthGuard('google'))
  googleLogin() {
    // initiates the Google OAuth2 login flow
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  googleLoginCallback(@Req() req, @Res() res)  {
    // handles the Google OAuth2 callback
    const jwt: string = req.user.jwt;
    if (jwt) {
      res.redirect('http://localhost:4200/login/succes/' + jwt);
    } else {
      res.redirect('http://localhost:4200/login/failure');
    }
  }

  @Post('signup')
  signup(@Body() signupRequestDto: SignupRequestDto): Promise<{token: string}> {
    return this.authService.performLocalSignup(signupRequestDto);
  }

  @Post('login')
  login(@Body() loginRequestDto: LoginRequestDto): Promise<{token: string}> {
    return this.authService.performLocalLogin(loginRequestDto);
  }

  @Get('me')
  @UseGuards(AuthGuard('jwt'))
  protectedResource(@CurrentUser() user) {
    return user;
  }
}
