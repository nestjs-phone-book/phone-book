import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { AuthService } from '../services/auth.service';
import { Provider } from '../services/oauth/provider.enum';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private readonly authService: AuthService) {
    super({
      clientID    : '222887788133-e91i875s7g2k1tdgmqidpeif3gdttf2t.apps.googleusercontent.com',
      clientSecret: '1loVvjHubDm2SYxMS4rmsQmV',
      callbackURL : 'http://localhost:3000/auth/google/callback',
      passReqToCallback: true,
      scope: ['profile', 'email'],
    });
  }

  // tslint:disable-next-line:ban-types
  async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
    try {
      const jwt: string = await this.authService.performOAuthLogin(profile, Provider.GOOGLE);
      const user = {
          jwt,
       };

      done(null, user);
    } catch (err) {
      // console.log(err)
      done(err, false);
    }
  }

}
