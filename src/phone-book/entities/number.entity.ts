import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Contact } from './contact.entity';

@Entity('numbers')
export class Number {

  // tslint:disable-next-line:variable-name
  constructor(number: string) {
    this.number = number;
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: string;

  @ManyToOne(type => Contact, contact => contact.numbers, {onDelete: 'CASCADE'})
  @JoinColumn({ name: 'contact_id' })
  contact: Contact;
}
