import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import {Number} from './number.entity';
import { Email } from './email.entity';

@Entity('contacts')
export class Contact {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  // tslint:disable-next-line:variable-name
  @OneToMany(type => Number, number => number.contact)
    // tslint:disable-next-line:ban-types
  numbers: Number[];

  @OneToMany(type => Email, email => email.contact)
    // tslint:disable-next-line:ban-types
  emails: Email[];
}
