import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Contact } from './contact.entity';

@Entity('emails')
export class Email {

  constructor(email: string) {
    this.email = email;
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @ManyToOne(type => Contact, contact => contact.emails, {onDelete: 'CASCADE'})
  @JoinColumn({ name: 'contact_id' })
  contact: Contact;
}
