import {registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";

@ValidatorConstraint({ async: false })
export class IsNumbersArrayConstraint implements ValidatorConstraintInterface {

  validate(numbers: string[], args: ValidationArguments) {
    let ok = true;
    // tslint:disable-next-line:variable-name
    for (const number of numbers) {
      if (!this.validateEmail(number)) {
        ok = false;
      }
    }
    return ok;
  }

  // tslint:disable-next-line:variable-name
  validateEmail(number): boolean {
    const re = /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/;
    return re.test(String(number).toLowerCase());
  }

}

export function IsNumbersArray(validationOptions?: ValidationOptions) {
  // tslint:disable-next-line:ban-types
  return (object: Object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsNumbersArrayConstraint,
    });
  };
}
