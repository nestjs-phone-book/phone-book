import {registerDecorator, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";

@ValidatorConstraint({ async: false })
export class IsEmailsArrayConstraint implements ValidatorConstraintInterface {

  validate(emails: string[], args: ValidationArguments) {
    let ok = true;
    for (const email of emails) {
      if (!this.validateEmail(email)) {
        ok = false;
      }
    }
    return ok;
  }

  validateEmail(email): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

}

export function IsEmailsArray(validationOptions?: ValidationOptions) {
  // tslint:disable-next-line:ban-types
  return (object: Object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsEmailsArrayConstraint,
    });
  };
}
