import { Injectable } from '@nestjs/common';
import { Contact } from '../entities/contact.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ContactsRepository } from '../repositories/contacts.repository';
import { NewContactDto } from '../dto/new-contact.dto';
import { Email } from '../entities/email.entity';
import { Number } from '../entities/number.entity';
import { NumbersRepository } from '../repositories/numbers.repository';
import { EmailsRepository } from '../repositories/emails.repository';
import { AbstractContactDto } from '../dto/abstract-contact.dto';
import { EditContactDto } from '../dto/edit-contact.dto';
import { DeleteResult } from 'typeorm';

@Injectable()
export class ContactsService {

  constructor(
    @InjectRepository(Contact) private contactsRepository: ContactsRepository,
    @InjectRepository(Number) private numbersRepository: NumbersRepository,
    @InjectRepository(Email) private emailsRepository: EmailsRepository,
  ) { }

  async getAllContacts(): Promise<Contact[]> {
    return await this.contactsRepository.find({relations: ['numbers', 'emails']});
  }

  async getContact(id: number): Promise<Contact> {
    return await this.contactsRepository.findOneOrFail(id, {relations: ['numbers', 'emails']});
  }

  async addContact(newContactDto: NewContactDto): Promise<Contact> {
    const c = new Contact();
    return await this.saveContact(c, newContactDto);
  }

  async updateContact(id: number , editContactDto: EditContactDto): Promise<Contact> {
    const c: Contact = await this.getContact(id);
    return await this.saveContact(c, editContactDto);
  }

  async deleteContact(id: number): Promise<DeleteResult> {
    const c: Contact = await this.getContact(id);
    return await this.contactsRepository.delete(c);
  }

  private async saveContact(c: Contact, contactDto: AbstractContactDto) {
    c.name = contactDto.name;
    c.emails = [];
    c.numbers = [];
    for (const email of contactDto.emails) {
      c.emails.push(await this.saveEmail(email));
    }
    // tslint:disable-next-line:variable-name
    for (const number of contactDto.numbers) {
      // @ts-ignore
      c.numbers.push(await this.saveNumber(number));
    }
    return await this.contactsRepository.save(c);
  }

  private async saveEmail(email: string) {
    return await this.emailsRepository.save(new Email(email));
  }

  // tslint:disable-next-line:variable-name
  private async saveNumber(number: string) {
    // tslint:disable-next-line:no-construct
    return await this.numbersRepository.save(new Number(number));
  }
}
