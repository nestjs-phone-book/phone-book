import {EntityRepository, Repository} from 'typeorm';
import { Number } from '../entities/number.entity';

@EntityRepository(Number)
// tslint:disable-next-line:ban-types
export class NumbersRepository extends Repository<Number> {

}
