import { IsArray, IsNotEmpty } from 'class-validator';
import { IsNumbersArray } from '../utils/validators/is-numbers-array';
import { IsEmailsArray } from '../utils/validators/is-emails-array';

export class AbstractContactDto {
    @IsNotEmpty()
    name: string;

    @IsArray()
    @IsNumbersArray()
    numbers: string[];

    @IsArray()
    @IsEmailsArray()
    emails: string[];
}
