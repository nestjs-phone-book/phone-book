import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Contact } from '../entities/contact.entity';
import { ContactsService } from '../services/contacts.service';
import { NewContactDto } from '../dto/new-contact.dto';
import { EditContactDto } from '../dto/edit-contact.dto';

@Controller('contacts')
export class ContactsController {
  constructor(private contactsService: ContactsService) {}

  @Get()
  getAllContacts() {
    return this.contactsService.getAllContacts();
  }

  @Get(':id')
  getContact(@Param('id') id: number) {
    return this.contactsService.getContact(id);
  }

  @Post()
  addContact(@Body() newContact: NewContactDto) {
    return this.contactsService.addContact(newContact);
  }

  @Put(':id')
  editContact(@Param('id') id: number, @Body() editContact: EditContactDto) {
    return this.contactsService.updateContact(id, editContact);
  }

  @Delete(':id')
  deleteContact(@Param('id') id: number) {
    return this.contactsService.deleteContact(id);
  }
}
